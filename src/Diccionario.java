public class Diccionario {
// Atributos de la clase
	
	public String [][] capitales = {
			{"chile","santiago"},
			{"venezuela","caracas"},
			{"peru","lima"},
	};
	
	String[][] ciudades = {
			{"valdivia","La perla del sur"},
			{"talcahuano","Es lindo pero huele mal"},
			{"santiago","Capital de Chile, Del asco "},
			{"concepcion","Ciudad grande (No tanto como Santiago)"},
			};
	
	private int anoPublicacion = 1996;

	public String retornarfecha() {
		return "19 de Junio de "+anoPublicacion;
		
	}
	
	
	public String buscarCapitales(String pais) {
		
		for (int i=0; i<capitales.length;i++) {
			if (capitales[i][0].equals(pais)) {
				return capitales[i][1];	
			}		
		}
		return "No se encuentra la capital entre la base de datos";
	}
	
	public String buscarDescripciones(String ciudadd) {		
		for (int i=0; i<ciudades.length;i++) {
			if (ciudades[i][0].equals(ciudadd)){
				return ciudades [i][1];
			}
		}
		return "No se encuentra la ciudad registrada";
	}
}